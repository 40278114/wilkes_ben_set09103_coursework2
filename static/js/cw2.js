$(document).ready(function (){
	$('#UserMessage').hide();
	$('#Enquire').click(function (){
		$('#Enquire').hide();
		$('#UserMessage').show();
  	});
	$('#TigerCheck').attr("checked",true);
	$('#LionCheck').attr("checked",true);
	$('#CheetahCheck').attr("checked",true);
	$('#LeopardCheck').attr("checked",true);
	$('#JaguarCheck').attr("checked",true);
	$('#LynxCheck').attr("checked",true);
	$('#CougarCheck').attr("checked",true);
	$('#MaleCheck').attr("checked",true);
	$('#FemaleCheck').attr("checked",true);

  	$('#TigerCheck').change(function () {
    		if (!this.checked){ 
      			$('.Tiger').hide();
    		}
    		if (this.checked){
      			$('.Tiger').show();
    		}
  	});

  	$('#LionCheck').change(function () {
    		if (!this.checked){ 
      			$('.Lion').hide();
    		}
    		if (this.checked){
      			$('.Lion').show();
    		}
  	});

  	$('#CheetahCheck').change(function () {
    		if (!this.checked){ 
      			$('.Cheetah').hide();
    		}
    		if (this.checked){
      			$('.Cheetah').show();
    		}
  	});

  	$('#LeopardCheck').change(function () {
    		if (!this.checked){ 
      			$('.Leopard').hide();
    		}
    		if (this.checked){
      			$('.Leopard').show();
    		}
  	});

  	$('#JaguarCheck').change(function () {
    		if (!this.checked){ 
      			$('.Jaguar').hide();
    		}
    		if (this.checked){
      			$('.Jaguar').show();
    		}
  	});

  	$('#LynxCheck').change(function () {
    		if (!this.checked){ 
      			$('.Lynx').hide();
    		}
    		if (this.checked){
      			$('.Lynx').show();
    		}
  	});

  	$('#CougarCheck').change(function () {
    		if (!this.checked){ 
      			$('.Cougar').hide();
    		}
    		if (this.checked){
      			$('.Cougar').show();
    		}
  	});

  	$('#MaleCheck').change(function () {
    		if (!this.checked){ 
      			$('.Male').hide();
    		}
    		if (this.checked){
      			$('.Male').show();
    		}
  	});

  	$('#FemaleCheck').change(function () {
    		if (!this.checked){ 
      			$('.Female').hide();
    		}
    		if (this.checked){
      			$('.Female').show();
    		}
  	});
});
