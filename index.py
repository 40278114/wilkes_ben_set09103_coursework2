from flask import Flask, render_template, url_for, request, session, redirect, flash
from flask_mail import Mail, Message
import sqlite3, os

app = Flask(__name__)
app.secret_key = '\x83}\xdc\xd0\xa3&\x80\xe3\x904\xc82\xdb+\xa7G\x9e\xfdc\xc9\x901^\x0c'

app.config.update(dict(
    MAIL_SERVER = 'smtp.googlemail.com',
    MAIL_PORT = 465,
    MAIL_USE_TLS = False,
    MAIL_USE_SSL = True,
    MAIL_USERNAME = 'bcwcustomerservice',
    MAIL_PASSWORD = 'BCWadmin17'
))

mail = Mail(app)

@app.route('//')
def index():
	return render_template('index.html', methods=['GET'])

@app.route('/about/', methods=['GET'])
def about():
	return render_template('about.html')

@app.route('/stock/', methods=['GET'])
def stock():
	con = sqlite3.connect('cw2.db')
	con.row_factory = sqlite3.Row
	cur = con.cursor()
	cur.execute("select * from BCW_cats")
	rows = cur.fetchall(); 
	cur.close
	con.close()
	return render_template('stock.html',rows = rows)

@app.route('/stock/showcat/', methods=['GET','POST'])
def showcat():
	if request.method == 'POST':
		catid = request.form['catid']
		con = sqlite3.connect('cw2.db')
		con.row_factory = sqlite3.Row
		cur = con.cursor()
		cur.execute("SELECT * FROM BCW_Cats WHERE Cat_ID = (?)", (catid,))
		rows = cur.fetchall();
		cur.close
		con.close()
		return render_template('showcat.html',rows = rows)
	else:
		return redirect('/stock/')

@app.route('/stock/showcat/usermessage/', methods=['POST'])
def usermessage():
	useremail = request.form['emailaddress']
	usermessage = request.form['message']
	catname = request.form['catname']
    	msg = Message('Customer Message from: '+useremail, sender='bcwcustomerservice@gmail.com', recipients=['bcwcustomerservice@gmail.com'])
    	msg.body = 'Responding to '+catname+'s advert. '+usermessage
    	mail.send(msg)
	flash('Message sent! We aim to respond within 2 working days.')
	return redirect('/stock/')

@app.route('/contact/', methods=['GET'])
def contact():
	return render_template('contact.html')

@app.route('/contact/contactmessage/', methods=['POST'])
def contactmessage():
	useremail = request.form['emailaddress']
	usermessage = request.form['message']
    	msg = Message('Customer Message from: '+useremail, sender='bcwcustomerservice@gmail.com', recipients=['bcwcustomerservice@gmail.com'])
    	msg.body = 'General message from site: '+usermessage
    	mail.send(msg)
	flash('Message sent! We aim to respond within 2 working days.')
	return redirect('/contact/')

@app.route('/admin/', methods=['GET', 'POST'])
def admin(Logged_in=None):
	
	try:
		if request.method == 'POST':
			attempted_username = request.form['username']
        		attempted_password = request.form['password']		
			con = sqlite3.connect('cw2.db')
			con.row_factory = sqlite3.Row
			cur = con.cursor()
			cur.execute("SELECT * FROM BCW_Admin WHERE BCW_un_admin = (?) AND BCW_pw_admin = (?)", (attempted_username, attempted_password))
			rows = cur.fetchone() 
			cur.close
			con.close()
			if rows is None:
				flash('Login failed')
				return render_template('admin.html')
			else:
				session['Logged_in'] = 'Logged_in'
				return redirect('/admin/console/')
		elif session['Logged_in'] == 'Logged_in':
			return redirect('/admin/console')
	except KeyError:
		pass
	return render_template('admin.html')

@app.route('/admin/console/', methods=['GET'])
def console():
	try:
		if session['Logged_in'] == 'Logged_in':
			return render_template('console.html')
	except KeyError:
		pass
	flash('You are not logged in')
	return redirect('/admin/')

@app.route('/admin/console/addnew/', methods=['GET', 'POST'])
def addnew():
	try:
		if request.method == 'POST':
			breed = request.form['breed']
        		name = request.form['name']
			sex = request.form['sex']
			age = request.form['age']
			neatured = request.form['neatured']
			houseT = request.form['housetrained']
			childF = request.form['childfriendly']
			location = request.form['location']
			price = request.form['price']
			descripton = request.form['description']
			photo = request.files['photo']
			photolocation = "static/uploads/"+photo.filename
			photo.save(photolocation)
			con = sqlite3.connect('cw2.db')
			con.row_factory = sqlite3.Row
			cur = con.cursor()
			cur.execute("INSERT INTO BCW_Cats VALUES (NULL,?,?,?,?,?,?,?,?,?,?,?)", (breed,name,sex,age,neatured,houseT,childF,location,price,descripton,photolocation))
			con.commit() 
			cur.close
			con.close()
			flash('Advert added successfully')
			return redirect('/admin/console/')
		elif session['Logged_in'] == 'Logged_in':
			return render_template('add.html')
	except KeyError:
		pass
	flash('You are not logged in')
	return redirect('/admin/')

@app.route('/admin/console/deleteadvert/', methods=['GET', 'POST'])
def deleteadvert():
	try:
		if request.method == 'POST':
			cat_id = request.form['delete']
			photo_location = request.form['photolocation']
			os.remove(photo_location)
			con = sqlite3.connect('cw2.db')
			con.row_factory = sqlite3.Row
			cur = con.cursor()
			cur.execute("DELETE FROM BCW_Cats WHERE CAT_ID = (?)", (cat_id,))
			con.commit() 
			cur.close
			con.close()
			flash('Advert deleted')
			return redirect('/admin/console/')
		elif session['Logged_in'] == 'Logged_in':
			con = sqlite3.connect('cw2.db')
			con.row_factory = sqlite3.Row
			cur = con.cursor()
			cur.execute("select * from BCW_cats")
			rows = cur.fetchall(); 
			cur.close
			con.close()
			return render_template('delete.html',rows = rows)
	except KeyError:
		pass
	flash('You are not logged in')
	return redirect('/admin/')


@app.route('/logout/', methods=['GET'])
def logout():
	session['Logged_in'] = None
	return redirect('//')

if __name__ == "__main__":
	app.run( host ='0.0.0.0 ', debug = True )